#include <Arduino.h>
#define vert D2

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(vert, OUTPUT);
}

void loop()
{
  int cpt = 500;
while(cpt > 0){
  // turn the LED on (HIGH is the voltage level)
  digitalWrite(vert, HIGH);
  // wait for a second
  delay(cpt);
  // turn the LED off by making the voltage LOW
  digitalWrite(vert, LOW);
  // wait for a second
  delay(cpt);
  cpt = cpt - 10 ;

}
}

