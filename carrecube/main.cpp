// Fichier "cetc.cpp"
// Application console
// Application qui permet de faire le carre et le cube d'un nombre
// cetc.exe =   cetc.cpp                ce fichier
// Alavoine et Crombe le 04/11/2020

//---------------------------------------------------------------------------


#pragma hdrstop
#include <iostream.h>                                           // pour cout , endl
//---------------------------------------------------------------------------

#pragma argsused
int main(int argc, char* argv[])
{

    int mn ;                                                     //variable mon nombre
    int carre ;                                                  //variable carre
    int cube ;                                                   //variable cube

    //Insertion du Nombre+ affichage
    cout << "valeur de mon nombre : " << endl ;                  //texte valeur mon nombre
    cin >> mn ;                                                  //entrer valeur mon nombre
    cout << "valeur de mon nombre = " << mn << endl ;            //afficher valeur mon nombre

    //Calcul+ affichage du nombre au carrée
    carre = mn * mn ;                                            //calcul au carre
    cout << "la valeur carre et de = " << carre << endl ;        //afficher valeur mon nombre au carre

    //Calcul+ affichage du nombre au cube
    cube = carre * mn ;                                          //calcul au cube
    cout << "la valeur au cube est de = " << cube ;              //affciher valeur mon nombre au cube


    return 0;
}
//---------------------------------------------------------------------------

